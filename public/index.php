<?php

require_once('../app/init.php');
if(!preg_match('/home/i', $_GET['url'])) {
    if(!empty($_SESSION['user_id'])) {
        header('Location: ' . BASE_URL . '/home');
    }
} else {
    if(empty($_SESSION['user_id'])) {
        header('Location: ' . BASE_URL . '/login');
    }
 }
$app = new App();
?>