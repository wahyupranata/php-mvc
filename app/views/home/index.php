<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Selamat Datang</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group me-2">
                <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
                <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
            </div>
            <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar" class="align-text-bottom"></span>
                This week
            </button>
        </div>
    </div>

    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
    <h2 class="fw-semibold">
        Selamat Datang
        <?php
        if(isset($data['name'])) {
            echo $data['name'];
        } elseif(isset($data['company'])) {
            echo $data['company'];
        }
        ?>
    </h2>
    <div class="d-flex justify-content-between align-items-center my-2">
        <h6>Berikut data user saat ini:</h6>
    </div>
    <div class="table-responsive text-center">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Name</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; foreach($data['users'] as $user): ?>
                <tr>
                    <td><?= $i++ ?></td>
                    <td><?= $user['first_name'] . " " . $user['last_name'] ?></td>
                    <td><?= $user['username'] ?></td>
                    <td><?= $user['email'] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</main>