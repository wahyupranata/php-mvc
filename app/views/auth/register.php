<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.101.0">
    <title>Register</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/sign-in/">

    <link href="<?= BASE_URL ?>/css/bootstrap.min.css" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .b-example-divider {
            height: 3rem;
            background-color: rgba(0, 0, 0, .1);
            border: solid rgba(0, 0, 0, .15);
            border-width: 1px 0;
            box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
        }

        .b-example-vr {
            flex-shrink: 0;
            width: 1.5rem;
            height: 100vh;
        }

        .bi {
            vertical-align: -.125em;
            fill: currentColor;
        }

        .nav-scroller {
            position: relative;
            z-index: 2;
            height: 2.75rem;
            overflow-y: hidden;
        }

        .nav-scroller .nav {
            display: flex;
            flex-wrap: nowrap;
            padding-bottom: 1rem;
            margin-top: -1px;
            overflow-x: auto;
            text-align: center;
            white-space: nowrap;
            -webkit-overflow-scrolling: touch;
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="<?= BASE_URL?>/css/signin.css" rel="stylesheet">
</head>
<body class="text-center">

<main class="form-register w-100 m-auto">
    <form action="<?= BASE_URL ?>/register/store" method="post">
        <img class="mb-4" src="<?= BASE_URL ?>/img/bootstrap-logo.svg" alt="" width="72" height="57">
        <h1 class="h3 mb-3 fw-normal">Register</h1>
        <?php Flasher::flash(); ?>

        <div class="form-floating">
            <input type="text" class="form-control" id="username" name="username" placeholder="name@example.com">
            <label for="username" class="text-secondary">Username</label>
        </div>
        <div class="form-floating">
            <input type="email" class="form-control rounded-0" id="email" name="email" placeholder="name@example.com">
            <label for="email" class="text-secondary">Email address</label>
        </div>
        <div class="form-floating">
            <input type="text" id="firstname" name="first_name" class="form-control rounded-0" placeholder="First name...">
            <label for="firstname" class="text-secondary">First Name</label>
        </div>
        <div class="form-floating">
            <input type="text" id="lastname" name="last_name" class="form-control rounded-0" placeholder="Last name...">
            <label for="lastname" class="text-secondary">Last Name</label>
        </div>
        <div class="form-floating">
            <input type="password" id="password" name="password" class="form-control rounded-0" placeholder="Last name...">
            <label for="password" class="text-secondary">Password</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="conf_password" name="conf_password" placeholder="Password Confirmation">
            <label for="confPassword" class="text-secondary">Password Confirmation</label>
        </div>

        <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
        <small>Sudah buat akun? <a href="<?= BASE_URL ?>/login">Login!</a></small>
        <p class="mt-5 mb-3 text-muted">&copy; Wahyu Pranata | 2022</p>
    </form>
</main>

<script src="<?=BASE_URL?>/js/bootstrap.min.js"></script>
</body>
</html>