<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="<?=BASE_URL?>/css/bootstrap.min.css">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .b-example-divider {
            height: 3rem;
            background-color: rgba(0, 0, 0, .1);
            border: solid rgba(0, 0, 0, .15);
            border-width: 1px 0;
            box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
        }

        .b-example-vr {
            flex-shrink: 0;
            width: 1.5rem;
            height: 100vh;
        }

        .bi {
            vertical-align: -.125em;
            fill: currentColor;
        }

        .nav-scroller {
            position: relative;
            z-index: 2;
            height: 2.75rem;
            overflow-y: hidden;
        }

        .nav-scroller .nav {
            display: flex;
            flex-wrap: nowrap;
            padding-bottom: 1rem;
            margin-top: -1px;
            overflow-x: auto;
            text-align: center;
            white-space: nowrap;
            -webkit-overflow-scrolling: touch;
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="<?= BASE_URL?>/css/signin.css" rel="stylesheet">
</head>
<body class="text-center">

<main class="form-login w-100 m-auto">
    <form action="<?= BASE_URL ?>/login/auth" method="post">
        <img class="mb-4" src="<?= BASE_URL ?>/img/bootstrap-logo.svg" alt="" width="72" height="57">
        <h1 class="text-center">Login</h1>
        <?php Flasher::flash(); ?>
        <div class="form-floating">
            <input type="email" class="form-control" id="floatingInput" name="email" placeholder="name@example.com" value="<?php if(isset($_COOKIE['logged_email'])) echo $_COOKIE['logged_email']  ?>">
            <label for="floatingInput" class="text-secondary">Email address</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="password" name="password" placeholder="Last name...">
            <label for="password" class="text-secondary">Password</label>
        </div>
        <div class="text-start mb-3">
            <input type="checkbox" class="form-check-input" id="rememberMe" name="remember_me" <?php if(isset($_COOKIE['logged_email'])) echo 'checked'?>>
            <label for="rememberMe" class="form-check-label text-dark">Ingat saya</label>
        </div>
        <button type="submit" class="w-100 btn btn-lg btn-primary">Masuk</button>
        <small>Belum punya akun? <a href="<?= BASE_URL ?>/register">Daftar!</a></small>
        <p class="mt-5 mb-3 text-muted">&copy; Wahyu Pranata | 2022</p>
    </form>
</main>

<script src="<?=BASE_URL?>/js/bootstrap.min.js"></script>

</body>
</html>