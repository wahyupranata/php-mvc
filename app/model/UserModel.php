<?php

class UserModel {
    private $table = 'users',
            $db,
            $name = 'Wahyu';
    public function getUser() {
        return $this->name;
    }
    public function __construct() {
        $this->db = new Database();
    }
    public function getAllUser() {
        $this->db->query("SELECT * FROM {$this->table}");
        return $this->db->resultAll();
    }
    public function getUserByID($id) {
        $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
        $this->db->bind('id', $id);
        return $this->db->resultSingle();
    }
    public function getUserByEmail($email) {
        $this->db->query("SELECT * FROM {$this->table} WHERE email=:email");
        $this->db->bind('email', $email);
        return $this->db->resultSingle();
    }
    public function register($data) {
        if($data['password'] === $data['conf_password']) {
            $password = password_hash(htmlspecialchars($data['password']), PASSWORD_BCRYPT);

            $this->db->query("INSERT INTO users (username, email, first_name, last_name, password) 
                                VALUES (:username, :email, :first_name, :last_name, :password)");

            $this->db->bind('username', htmlspecialchars($data['username']));
            $this->db->bind('email', htmlspecialchars($data['email']));
            $this->db->bind('first_name', htmlspecialchars($data['first_name']));
            $this->db->bind('last_name', htmlspecialchars($data['last_name']));
            $this->db->bind('password', $password);

            $this->db->execute();
            return $this->db->rowCount();
        } else {
            header('Location: ' . BASE_URL . '/register');
        }
    }
    public function login($email, $password) {
        $this->db->query("SELECT * FROM users WHERE email = :email");
        $this->db->bind('email', $email);

        $row = $this->db->resultSingle();
        if($row) {
            $hashedPassword = $row['password'];
            if(password_verify($password, $hashedPassword)) {
                return $row;
            } else {
                return false;
            }
        }
    }
}