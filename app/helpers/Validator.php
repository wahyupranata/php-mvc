<?php

class Validator {
    public static function validateEmail($input) {
        if(empty($input) && !filter_var($input, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }
    public static function validatePassword($input) {
        if(empty($input) && strlen($input) < 8) {
            return false;
        }
        return true;

    }
    public static function validateName($input) {
        if(empty($input) && !preg_match("^[A-Za-z][A-Za-z0-9_]$", $input) && strlen($input) > 32 ) {
            return false;
        }
        return true;
    }
}

