<?php
require_once "../app/core/Controller.php";

class Home extends Controller {

    public function __construct() {
        if(empty($_SESSION['user_id'])) {
            Flasher::setFlash("Harap login dulu", "danger");
            header('Location: ' . BASE_URL . '/login');
            exit;
        }
    }
    public function index() {
        $data['title'] = 'Home';
        $data['users'] = $this->model('UserModel')->getAllUser();
        $this->view('template/header', $data);
        $this->view('components/sidebar');
        $this->view("home/index", $data);
        $this->view("components/modal");
        $this->view('template/footer', $data);
    }
    public function about($company = 'SMKN 1 Denpasar') {
        $data['title'] = 'about';
        $data['company'] = $company;
        $data['users'] = $this->model('UserModel')->getAllUser();
        $data['name'] = $_SESSION['name'];
        $this->view('template/header', $data);
        $this->view('components/sidebar');
        $this->view("home/index", $data);
        $this->view('template/footer', $data);
    }
    public function store() {
        if( $this->model('UserModel')->createUser($_POST) > 0 ) {
            Flasher::setFlash("berhasil", "ditambahkan", "success");
            header('Location: ' . BASE_URL . '/home/index');
            exit;
        } else {
            Flasher::setFlash("gagal", "ditambahkan", "danger");
            header('Location: ' . BASE_URL . '/home/index');
            exit;
        }
    }
}