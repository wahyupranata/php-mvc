<?php
require_once "../app/core/Controller.php";

class Register extends Controller
{
    public function __construct() {
        if(!empty($_SESSION['user_id'])) {
            header('Location: ' . BASE_URL . '/home');
        }
    }

    public function index()
    {
        $this->view('auth/register');
    }

    public function store()
    {

        if(empty($_POST['username']) && empty($_POST['email']) && empty($_POST['first_name']) && empty($_POST['last_name']) && empty($_POST['password']) && empty($_POST['conf_password'])) {
            Flasher::setFlash('Harap isi semua data!', '', 'warning');
            header("Location: " . BASE_URL . '/register');
            exit;
        }
        $usernameValidator = Validator::validateName($_POST['username']);
        $emailValidator = Validator::ValidateEmail($_POST['email']);
        $firstNameValidator = Validator::ValidateName($_POST['first_name']);
        $lastNameValidator = Validator::validateName($_POST['last_name']);
        $passwordValidator = Validator::validatePassword($_POST['password']);
        $confirmPasswordValidator = Validator::validatePassword($_POST['conf_password']);

        if(!$usernameValidator) {
            Flasher::setFlash('Username anda tidak valid!', 'danger');
            header("Location: " . BASE_URL . '/register');
            exit;
        } elseif(!$firstNameValidator || !$lastNameValidator) {
            Flasher::setFlash('Nama anda tidak valid!', 'danger');
            header('Location: ' . BASE_URL . '/register');
            exit;
        } elseif(!$emailValidator) {
            Flasher::setFlash('Email anda tidak valid!', 'danger');
            header('Location: ' . BASE_URL . '/register');
            exit;
        } elseif(!$passwordValidator || !$confirmPasswordValidator) {
            Flasher::setFlash('Password minimal 8 karakter!',  'danger');
            header('Location: ' . BASE_URL . '/register');
            exit;
        } else {
            $sameEmail = $this->model('UserModel')->getUserByEmail($_POST['email']);
            if ($sameEmail) {
                Flasher::setFlash("Anda sudah daftar sebelumnya, silahkan login <a href='" . BASE_URL ."/login'>Disini</a>", 'info');
                header('Location: ' . BASE_URL . '/register');
                exit;
            } elseif($_POST['password'] !== $_POST['conf_password']) {
                Flasher::setFlash('Password dan konfirmasi password tidak sesuai!', 'danger');
                header('Location: '. BASE_URL . '/register');
                exit;
            } else {
                if ($this->model('UserModel')->register($_POST) > 0) {
                    Flasher::setFlash('Anda berhasil register', 'success');
                    header('Location: ' . BASE_URL . '/login');
                    exit;
                }
            }
        }
    }
}