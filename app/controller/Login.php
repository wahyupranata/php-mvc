<?php
require_once "../app/core/Controller.php";

class Login extends Controller {
    public function __construct() {
        if(!empty($_SESSION['user_id'])) {
            header('Location: ' . BASE_URL . '/home');
        }
    }
    public function index() {
        $this->view('auth/login');
    }
    public function auth() {
        // Validasi Email & Password
        $emailValidator = Validator::ValidateEmail($_POST['email']);
        $passwordValidator = Validator::validatePassword($_POST['password']);

        // Cek jika password/email keliru
        if(empty($_POST['email']) && empty($_POST['password'])) {
            Flasher::setFlash('Mohon isi semua data!', 'warning');
            header('Location: ' . BASE_URL . '/login');
            exit;
        } elseif(!$emailValidator || !$passwordValidator) {
            Flasher::setFlash('Data yang anda kirimkan keliru!', 'danger');
            header('Location: ' . BASE_URL . '/login');
            exit;
        } else {
            $loggedUser = $this->model('UserModel')->login($_POST['email'], $_POST['password']);
            if($loggedUser) {
                if(isset($_POST['remember_me'])) {
                    $this->setRememberMe();
                } else {
                    setcookie('logged_email', '', time() - 1);
                }
                $this->createUserSession($loggedUser);
            } else {
                Flasher::setFlash('Username atau Password salah, silakan coba lagi', 'danger');
                header('Location: '. BASE_URL . '/login');
            }
        }
    }
    private function createUserSession($user) {
        $_SESSION['user_id'] = $user['id'];
        $_SESSION['username'] = $user['username'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['name'] = $user['first_name'] . ' ' . $user['last_name'];
        Flasher::setFlash('Anda berhasil login', 'info');
        header("Location: ". BASE_URL . "/home/about");
        exit;
    }
    private function setRememberMe() {
        setcookie('logged_email', $_POST['email'], time() + (86400 * 30));
    }
    public function logout() {
        $_SESSION = array();
        Flasher::setFlash('Anda berhasil logout', "info");
        header('Location: '. BASE_URL . '/login');
    }
}